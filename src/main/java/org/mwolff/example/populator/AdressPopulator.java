package org.mwolff.example.populator;

import org.mwolff.converter.Populator;
import org.mwolff.example.data.UserData;
import org.mwolff.example.entities.Adresse;

public class AdressPopulator implements Populator<Adresse, UserData> {

    @Override
    public void convert(Adresse source, UserData target) {

        // Please add your conversions here
        //if (source instanceof Adresse) {
            target.setMystrasse(((Adresse )source).getStrasse());
            target.setMyplz(((Adresse )source).getPlz());
            target.setMyort(((Adresse )source).getOrt());
            target.setMyhausnummer(((Adresse )source).getHausnummer());
        //}
    }

    @Override
    public String returnType() {
        return Adresse.class.toString();
    }
}