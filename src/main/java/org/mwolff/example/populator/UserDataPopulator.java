package org.mwolff.example.populator;

import org.mwolff.converter.Populator;
import org.mwolff.example.data.UserData;
import org.mwolff.example.entities.Customer;

public class UserDataPopulator implements Populator<Customer, UserData> {

    @Override
    public void convert(Customer source, UserData target) {

        // Please add your conversions here
        if (source instanceof Customer) {
            target.setName(((Customer )source).getLastname());
            target.setPrename(((Customer )source).getFirstname());
        }
    }

    @Override
    public String returnType() {
        return Customer.class.toString();
    }
}