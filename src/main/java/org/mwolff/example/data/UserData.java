/**
This is a generated class. Please don't change. Changes will be overwritten due next compile cycle.
Generated from Simple Bean Generator 2.0.3
Template is: class-template.vm
*/
package org.mwolff.example.data;

import org.mwolff.converter.ViewDTO;

public class UserData implements ViewDTO {

    // Factory Method
    static public UserData getNewInstance() {
        return new UserData ();
    }

    // Properties
    private String prename;
    private String name;
    private String mystrasse;
    private String myhausnummer;
    private String myplz;
    private String myort;

    // Get- / Set operations
    public String getPrename() {
        return this.prename;
    }

    public void setPrename(final String prename) {
        this.prename = prename;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getMystrasse() {
        return this.mystrasse;
    }

    public void setMystrasse(final String mystrasse) {
        this.mystrasse = mystrasse;
    }

    public String getMyhausnummer() {
        return this.myhausnummer;
    }

    public void setMyhausnummer(final String myhausnummer) {
        this.myhausnummer = myhausnummer;
    }

    public String getMyplz() {
        return this.myplz;
    }

    public void setMyplz(final String myplz) {
        this.myplz = myplz;
    }

    public String getMyort() {
        return this.myort;
    }

    public void setMyort(final String myort) {
        this.myort = myort;
    }

}
