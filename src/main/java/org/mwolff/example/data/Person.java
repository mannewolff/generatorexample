/**
This is a generated class. Please don't change. Changes will be overwritten due next compile cycle.
Generated from Simple Bean Generator 2.0.3
Template is: class-template.vm
*/
package org.mwolff.example.data;


public class Person {

    // Factory Method
    static public Person getNewInstance() {
        return new Person ();
    }

    // Properties
    private String firstname;
    private String lastname;

    // Get- / Set operations
    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(final String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(final String lastname) {
        this.lastname = lastname;
    }

}
