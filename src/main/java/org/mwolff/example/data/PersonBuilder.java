/**
This is a generated class. Please don't change. Changes will be overwritten due next compile cycle.
Generated from Simple Bean Generator 2.0.3
Template is: test-class-template.vm
*/
package org.mwolff.example.data;


public class PersonBuilder {

    private Person person;

    // Factory Method
    private void createInstance() {
        person = new Person ();
    }

    public PersonBuilder () {
        createInstance();
    }

    // Set-Operations (with ...)
    public PersonBuilder withFirstname(String firstname) {
        person.setFirstname (firstname);
        return this;
    }
    public PersonBuilder withLastname(String lastname) {
        person.setLastname (lastname);
        return this;
    }
    public Person build() {
        return  person;
    }
}

