package org.mwolff.builder;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.SecureRandom;

import org.apache.commons.lang3.RandomUtils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class RandomTestNumber {

  private static final SecureRandom RANDOM = new SecureRandom();

  /**
   * @return positive random Long, not null
   */
  public static Long randomLong() {
    return Long.valueOf(randomlong());
  }

  /**
   * @param max
   *          the maximum value (inclusive)
   * @return postive random Long between 0 and max, not null
   */
  public static Long randomLong(final long max) {
    return Long.valueOf(RandomUtils.nextLong(0, max));
  }

  /**
   * @return positive random long
   */
  public static long randomlong() {
    return RandomUtils.nextLong();
  }

  /**
   * @return positive random Integer, not null
   */
  public static Integer randomInteger() {
    return Integer.valueOf(RANDOM.nextInt(Integer.MAX_VALUE));
  }

  /**
   * @param max
   *          the maximum value
   * @return positive random Integer between 0 and max, not null
   */
  public static Integer randomInteger(final int max) {
    return Integer.valueOf(randomInt(max));
  }

  /**
   * @param max
   *          the maximum value
   * @return positive random int between 0 and max, not null
   */
  public static int randomInt(final int max) {
    return RANDOM.nextInt(max + 1);
  }

  /**
   * @param min
   *          the minimum value
   * @param max
   *          the maximum value, has to be greater than min
   * @return positive random Integer between min and max
   */
  public static int randomInt(final int min,
                              final int max) {
    return RANDOM.nextInt((max - min) + 1) + min;
  }

  /**
   * @return positive random float
   */
  public static float randomfloat() {
    float floatValue = RANDOM.nextFloat();
    if (floatValue < 0) {
      floatValue *= -1.0f;
    }
    return floatValue;
  }

  /**
   * @return positive random {@link Float}, not null
   */
  public static Float randomFloat() {
    return Float.valueOf(randomfloat());
  }

  /**
   * @return positive random {@link BigDecimal} not null
   */
  public static BigDecimal randomBigDecimal() {
    return BigDecimal.valueOf(randomfloat()).setScale(2, RoundingMode.HALF_UP);
  }

  /**
   * @return positive random double
   */
  public static double randomdouble() {
    double doubleValue = RANDOM.nextDouble();
    if (doubleValue < 0) {
      doubleValue *= -1.0f;
    }
    return doubleValue;
  }

  /**
   * @return positive random {@link Double}, not null
   */
  public static Double randomDouble() {
    return Double.valueOf(randomdouble());
  }

}
