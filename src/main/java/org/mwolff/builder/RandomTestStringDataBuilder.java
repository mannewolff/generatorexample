package org.mwolff.builder;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class RandomTestStringDataBuilder {
  // MagicNumberCheck
  // CHECKSTYLE:DISABLE MagicNumberCheck FOR NEXT 75 LINES

  /**
   * @param maxLength
   *          the max length of random string to create
   * @return random string, not null
   */
  public static String randomString(final int maxLength) {
    final int length = RandomTestNumber.randomInteger(maxLength).intValue();
    return RandomStringUtils.random(length);
  }

  /**
   * @return random string with length limit of 10 chars, not null
   */
  public static String randomAlphabetic() {
    return RandomStringUtils.randomAlphabetic(10);
  }

  /**
   * @return random string with length limit of 10 chars, not null
   */
  public static String randomString() {
    return RandomStringUtils.random(10);
  }

  /**
   * @param maxLength
   *          the max length of random string to create
   * @return random string, not null
   */
  public static String randomAlphabetic(final int maxLength) {
    final int length = RandomTestNumber.randomInteger(maxLength).intValue();
    return RandomStringUtils.randomAlphabetic(length);
  }

  /**
   * @param min
   *          length
   * @param max
   *          length
   * @return random string with given min and max length, not null
   */
  public static String randomAlphabetic(final int min,
                                        final int max) {
    return RandomStringUtils.randomAlphabetic(min, max);
  }

  /**
   * @return random valid email
   */
  public static String randomEmail() {

    final String[] tldArray = new String[] { "com", "net", "org", "de" };
    final String user = RandomStringUtils.randomAlphabetic(10).toLowerCase();
    final String domain = RandomStringUtils.randomAlphabetic(10).toLowerCase();
    final String tld = tldArray[RandomTestNumber.randomInteger(tldArray.length - 1).intValue()];

    return user + "@" + domain + "." + tld;
  }

  /**
   * Random numeric string of size 10.
   *
   * @return not null
   */
  public static String randomNumeric() {
    return RandomStringUtils.randomNumeric(10);
  }

  /**
   * Random numeric string with given max length.
   *
   * @param max
   *          not null
   * @return not null
   */
  public static String randomNumeric(final int max) {
    return RandomStringUtils.randomNumeric(RandomTestNumber.randomInteger(max - 1).intValue() + 1);
  }

  /**
   * Random numeric string with given min and max length.
   *
   * @param min
   *          not null
   * @param max
   *          not null
   * @return not null
   */
  public static String randomNumeric(final int min,
                                     final int max) {
    return RandomStringUtils.randomNumeric(RandomTestNumber.randomInt(min, max));
  }

  public static byte[] randomBytes() {
    return RandomUtils.nextBytes(16);
  }

}
