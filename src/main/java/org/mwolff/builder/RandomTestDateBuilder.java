package org.mwolff.builder;

import java.security.SecureRandom;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class RandomTestDateBuilder {

  private static long beginTime = Timestamp.valueOf("1960-01-01 00:00:00").getTime();
  private static long endTime = new Date().getTime();

  private static final SecureRandom RANDOM = new SecureRandom();

  /**
   * @return random date in the past
   */
  public static java.sql.Date randomPastSqlDate() {
    final long diff = endTime - beginTime + 1;
    final long milliseconds = beginTime + (long) (RANDOM.nextDouble() * diff);
    return new java.sql.Date(milliseconds);
  }

  /**
   * @return random timestamp in the past
   */
  public static Timestamp randomPastTimestamp() {
    final long diff = endTime - beginTime + 1;
    final long milliseconds = beginTime + (long) (RANDOM.nextDouble() * diff);
    return new Timestamp(milliseconds);
  }

  /**
   * @return random past local date time
   */
  public static LocalDateTime randomPastLocalDateTime() {
    return randomPastTimestamp().toLocalDateTime();
  }

  /**
   * @return random past local date time
   */
  public static ZonedDateTime randomPastZonedDateTime() {
    return randomPastTimestamp().toLocalDateTime().atZone(ZoneId.systemDefault());
  }

  /**
   * @return random past local date time
   */
  public static OffsetDateTime randomPastOffsetDateTime() {
    return OffsetDateTime.now().minusDays(RANDOM.nextInt(5)).minusHours(RANDOM.nextInt(5))
        .minusMinutes(RANDOM.nextInt(5));
  }

  /**
   * @return random past local date
   */
  public static java.time.LocalDate randomPastLocalDate() {
    return randomPastTimestamp().toLocalDateTime().toLocalDate();
  }

  /**
   * @return random local time
   */
  public static java.time.LocalTime randomLocalTime() {
    return randomPastTimestamp().toLocalDateTime().toLocalTime();
  }

}
