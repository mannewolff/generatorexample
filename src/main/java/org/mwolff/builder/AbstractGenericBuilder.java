package org.mwolff.builder;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.util.ReflectionUtils;

/**
 * @param <TYPE>
 *          type parameter for the instance to build
 */
public abstract class AbstractGenericBuilder<TYPE> implements WithRandomTestData, Builder<TYPE> {

  /** This object will be filled and returned as the build product. */
  private final TYPE instanceToBuild;

  /**
   * Default Constructor creates the instance to build.
   */
  protected AbstractGenericBuilder() {
    this.instanceToBuild = this.createNewInstance();
  }

  /** {@inheritDoc} */
  @Override
  public TYPE build() {
    return this.instanceToBuild;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public TYPE lazyBuild() {
    return lazy().build();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<TYPE> lazyListBuild() {
    final List<TYPE> elements = new ArrayList<>();
    for (int i = 0; i < randomInt(2, 10); i++) {
      elements.add(createNewInstanceWithRandomValues());
    }
    return elements;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<TYPE> lazyListBuild(final int numberOfElements) {
    final List<TYPE> elements = new ArrayList<>();
    for (int i = 0; i < numberOfElements; i++) {
      elements.add(createNewInstanceWithRandomValues());
    }
    return elements;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Set<TYPE> lazySetBuild() {
    final HashSet<TYPE> elements = new HashSet<>();
    for (int i = 0; i < randomInt(2, 10); i++) {
      elements.add(createNewInstanceWithRandomValues());
    }
    return elements;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Set<TYPE> lazySetBuild(final int numberOfElements) {
    final Set<TYPE> elements = new HashSet<>();
    for (int i = 0; i < numberOfElements; i++) {
      elements.add(createNewInstanceWithRandomValues());
    }
    return elements;
  }

  private TYPE createNewInstanceWithRandomValues() {
    final TYPE newInstance = this.createNewInstance();
    this.fillWithRandomValues(newInstance);
    return newInstance;
  }

  /**
   * @return {@link #instanceToBuild}, not null
   */
  protected TYPE getInstanceToBuild() {
    return this.instanceToBuild;
  }

  /**
   * By this method only accessible fields for the {@link #instanceToBuild} are set.
   *
   * @return the builder instance
   */
  protected AbstractGenericBuilder<TYPE> withRandomAccessibleData() {
    final List<PropertyDescriptor> beanProperties = ReflectionUtil.determineJavaBeanProperties(
        this.instanceToBuild.getClass());

    for (final PropertyDescriptor propertyDescriptor : beanProperties) {
      this.writeRandomPropertyValue(propertyDescriptor);
    }
    return this;
  }

  /**
   * By this method all fields for the {@link #instanceToBuild} are set.
   *
   * @return the builder instance
   */
  protected AbstractGenericBuilder<TYPE> withRandomFieldValues() {
    this.fillWithRandomValues(this.instanceToBuild);
    return this;
  }

  /**
   * @param toBuild
   *          object which will be filled with random values
   */
  private void fillWithRandomValues(final TYPE toBuild) {
    final List<Field> beanProperties = ReflectionUtil.determineAllFields(toBuild.getClass());

    for (final Field field : beanProperties) {
      final Object propertyValue = determinePropertyValue(field.getType());
      if (propertyValue != null) {
        field.setAccessible(true);
        ReflectionUtils.setField(field, toBuild, propertyValue);
      }
    }
    this.afterFillWithRandomValues(toBuild);
  }

  /**
   * @param toBuild
   *          object which was filled with random values
   */
  protected void afterFillWithRandomValues(final TYPE toBuild) {

  }

  /**
   * @param propertyDescriptor
   *          not null
   */
  private void writeRandomPropertyValue(final PropertyDescriptor propertyDescriptor) {
    final Class<?> propertyType = propertyDescriptor.getPropertyType();
    final Object propertyValue = determinePropertyValue(propertyType);
    try {
      if (propertyValue != null) {
        propertyDescriptor.getWriteMethod().invoke(this.instanceToBuild, propertyValue);
      }
    } catch (final IllegalArgumentException | IllegalAccessException
        | InvocationTargetException cause) {
      throw new RuntimeException("could not set value " + propertyDescriptor.getName(), cause);
    }
  }

  /**
   * Creates a new instance of the class represented by this Class object. The class is instantiated
   * as if by a new expression with an empty argument list.
   *
   * @return not null
   */
  private TYPE createNewInstance() {
    final ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
    @SuppressWarnings("unchecked")
    final Class<TYPE> clazz = (Class<TYPE>) type.getActualTypeArguments()[0];
    try {
      return clazz.getDeclaredConstructor().newInstance();
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException | NoSuchMethodException | SecurityException cause) {
      throw new RuntimeException("could not create an instance for the builder", cause);
    }
  }

}
