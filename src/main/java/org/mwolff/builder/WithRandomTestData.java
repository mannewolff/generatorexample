package org.mwolff.builder;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

public interface WithRandomTestData extends RandomTestDataProvider {

  @Override
  default Long randomLong() {
    return RandomTestNumber.randomLong();
  }

  @Override
  default Long randomLong(final long max) {
    return RandomTestNumber.randomLong(max);
  }

  @Override
  default long randomlong() {
    return RandomTestNumber.randomlong();
  }

  @Override
  default Float randomFloat() {
    return RandomTestNumber.randomFloat();
  }

  @Override
  default Double randomDouble() {
    return RandomTestNumber.randomDouble();
  }

  @Override
  default Integer randomInteger() {
    return RandomTestNumber.randomInteger();
  }

  @Override
  default Integer randomInteger(final int max) {
    return RandomTestNumber.randomInteger(max);
  }

  @Override
  default int randomInt(final int min,
                        final int max) {
    return RandomTestNumber.randomInt(min, max);
  }

  @Override
  default int randomInt() {
    return this.randomInteger().intValue();
  }

  @Override
  default boolean randomboolean() {
    return this.randomInteger(1).intValue() == 0;
  }

  @Override
  default Boolean randomBoolean() {
    return Boolean.valueOf(randomboolean());
  }

  @Override
  default String randomString() {
    return RandomTestStringDataBuilder.randomString();
  }

  @Override
  default String randomString(final int maxLength) {
    return RandomTestStringDataBuilder.randomString(maxLength);
  }

  @Override
  default String randomAlphabetic() {
    return RandomTestStringDataBuilder.randomAlphabetic();
  }

  @Override
  default String randomAlphabetic(final int maxLength) {
    return RandomTestStringDataBuilder.randomAlphabetic(maxLength);
  }

  @Override
  default String randomAlphabetic(final int min,
                                  final int max) {
    return RandomTestStringDataBuilder.randomAlphabetic(min, max);
  }

  @Override
  default String randomNumeric() {
    return RandomTestStringDataBuilder.randomNumeric();
  }

  @Override
  default String randomNumeric(final int max) {
    return RandomTestStringDataBuilder.randomNumeric(max);
  }

  @Override
  default String randomNumeric(final int min,
                               final int max) {
    return RandomTestStringDataBuilder.randomNumeric(min, max);
  }

  @Override
  default <E extends Enum<E>> E randomEnum(final Class<E> enumType) {
    return RandomTestEnumDataBuilder.randomEnum(enumType);
  }

  @Override
  @SuppressWarnings("unchecked")
  default <T extends Enum<T>> T randomEnumExcept(final Class<T> enumType,
                                                 final T... excluded) {
    return RandomTestEnumDataBuilder.randomEnumExcept(enumType, excluded);
  }

  @Override
  default <T> T randomOf(final List<T> list) {
    return RandomOfUtils.randomOf(list);
  }

  @Override
  default <T> T randomOf(@SuppressWarnings("unchecked") final T... objects) {
    return RandomOfUtils.randomOf(objects);
  }

  @Override
  default String randomEmail() {
    return RandomTestStringDataBuilder.randomEmail();
  }

  @Override
  default Date randomPastSqlDate() {
    return RandomTestDateBuilder.randomPastSqlDate();
  }

  @Override
  default Timestamp randomPastTimestamp() {
    return RandomTestDateBuilder.randomPastTimestamp();
  }

  @Override
  default LocalDateTime randomPastLocalDateTime() {
    return RandomTestDateBuilder.randomPastLocalDateTime();
  }

  @Override
  default ZonedDateTime randomPastZonedDateTime() {
    return RandomTestDateBuilder.randomPastZonedDateTime();
  }

  @Override
  default OffsetDateTime randomPastOffsetDateTime() {
    return RandomTestDateBuilder.randomPastOffsetDateTime();
  }

  @Override
  default LocalDate randomPastLocalDate() {
    return RandomTestDateBuilder.randomPastLocalDate();
  }

  @Override
  default LocalTime randomLocalTime() {
    return RandomTestDateBuilder.randomLocalTime();
  }

  @Override
  default Calendar randomPastCalendar() {
    final Calendar instance = Calendar.getInstance();
    instance.setTimeInMillis(randomPastTimestamp().getTime());
    return instance;
  }

  @Override
  default BigDecimal randomBigDecimal() {
    return RandomTestNumber.randomBigDecimal();
  }

  @Override
  default byte[] randomBytes() {
    return RandomTestStringDataBuilder.randomBytes();
  }

  @Override
  default UUID randomUuid() {
    return UUID.randomUUID();
  }

  @Override
  default String randomIban() {
    return IBANValues.randomIBAN();
  }

  @Override
  default String randomInterleaved2Of5Barcode() {
    return randomNumeric(24, 24);
  }

  /**
   * @param propertyType
   *          the Class object that represents the Java type info
   * @return propertyValue, can be null
   */
  // CyclomaticComplexity
  // CHECKSTYLE:DISABLE CyclomaticComplexity FOR NEXT 3 LINES
  @SuppressWarnings("squid:S3776")
  default Object determinePropertyValue(final Class<?> propertyType) {

    Object propertyValue = null;
    if (propertyType == String.class) {
      propertyValue = this.randomAlphabetic();
    } else if (propertyType == Boolean.class || propertyType == boolean.class) {
      propertyValue = randomBoolean();
    } else if (propertyType == Integer.class || propertyType == int.class) {
      propertyValue = this.randomInteger();
    } else if (propertyType == Float.class || propertyType == float.class) {
      propertyValue = randomFloat();
    } else if (propertyType == Double.class || propertyType == double.class) {
      propertyValue = randomDouble();
    } else if (propertyType == Long.class || propertyType == long.class) {
      propertyValue = randomLong();
    } else if (propertyType == Timestamp.class) {
      propertyValue = randomPastTimestamp();
    } else if (propertyType == Date.class) {
      propertyValue = randomPastSqlDate();
    } else if (propertyType.isEnum()) {
      propertyValue = RandomTestEnumDataBuilder.random(propertyType);
    } else if (propertyType == LocalDateTime.class) {
      propertyValue = randomPastLocalDateTime();
    } else if (propertyType == LocalDate.class) {
      propertyValue = randomPastLocalDate();
    } else if (propertyType == ZonedDateTime.class) {
      propertyValue = randomPastZonedDateTime();
    } else if (propertyType == BigDecimal.class) {
      propertyValue = randomBigDecimal();
    } else if (propertyType == UUID.class) {
      propertyValue = UUID.randomUUID();
    }
    return propertyValue;
  }

}
