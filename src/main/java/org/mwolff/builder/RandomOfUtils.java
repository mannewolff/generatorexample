package org.mwolff.builder;

import java.util.List;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class RandomOfUtils {

  public static <T> T randomOf(final List<T> list) {
    return list.get(RandomTestNumber.randomInt(0, list.size() - 1));
  }

  public static <T> T randomOf(@SuppressWarnings("unchecked") final T... objects) {
    return objects[RandomTestNumber.randomInteger(objects.length - 1).intValue()];
  }

}
