package org.mwolff.builder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class RandomTestEnumDataBuilder {

  /**
   * @param <E>
   *          type parameter for the enum
   * @param enumType
   *          enum class
   * @return random enum
   */
  public static <E extends Enum<E>> E randomEnum(final Class<E> enumType) {

    final E[] types = enumType.getEnumConstants();
    final int index = RandomTestNumber.randomInt(types.length - 1);
    return types[index];
  }

  @SuppressWarnings("unchecked")
  public static <E extends Enum<E>> E randomEnumExcept(final Class<E> enumType,
                                                       final E... excluded) {

    final List<E> excludedEnumConstants = Arrays.asList(excluded);
    final List<E> selectableEnumConstants = new ArrayList<>();

    for (final E currentEnum : enumType.getEnumConstants()) {
      if (!excludedEnumConstants.contains(currentEnum)) {
        selectableEnumConstants.add(currentEnum);
      }
    }

    if (!selectableEnumConstants.isEmpty()) {
      return selectableEnumConstants
          .get(RandomTestNumber.randomInt(selectableEnumConstants.size() - 1));
    }
    throw new IllegalArgumentException("No enum selectable based on given arguments");
  }

  /**
   * Prefer to use randomEnum(Class). This random method is not type safe. Needed for the generic
   * builder to set property values by reflection.
   *
   * @param enumType
   *          enum class
   * @return random enum, not null
   */
  public static Object random(final Class<?> enumType) {

    final Object[] types = enumType.getEnumConstants();
    final int index = RandomTestNumber.randomInt(types.length - 1);
    return types[index];

  }
}
