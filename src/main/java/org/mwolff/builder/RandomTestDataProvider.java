package org.mwolff.builder;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

public interface RandomTestDataProvider {

  /**
   * @return true or false
   */
  boolean randomboolean();

  /**
   * @return {@link Boolean#TRUE} or {@link Boolean#FALSE}
   */
  Boolean randomBoolean();

  /**
   * @return positive random Integer, not null
   */
  Integer randomInteger();

  /**
   * @param max
   *          the maximum value
   * @return positive random Integer between 0 and max, not null
   */
  Integer randomInteger(int max);

  /**
   * @param min
   *          the minimum value
   * @param max
   *          the maximum value, has to be greater than min
   * @return positive random Integer between min and max
   */
  int randomInt(int min,
                int max);

  /**
   * @return positive random Long, not null
   */
  Long randomLong();

  /**
   * @param max
   *          the maximum value
   * @return positive random Long between 0 and max, not null
   */
  Long randomLong(long max);

  /**
   * @return positive random long
   */
  long randomlong();

  /**
   * @return positive random Integer, not null
   */
  int randomInt();

  /**
   * @return positive random Float, not null
   */
  Float randomFloat();

  /**
   * @return positive random Double, not null
   */
  Double randomDouble();

  /**
   * @return random string with length limit of 10 chars, not null
   */
  String randomString();

  /**
   * @param maxLength
   *          the max length of random string to create
   * @return random string, not null
   */
  String randomString(int maxLength);

  /**
   * @return random string with length limit of 10 chars, not null
   */
  String randomAlphabetic();

  /**
   * @param maxLength
   *          the max length of random string to create
   * @return random string, not null
   */
  String randomAlphabetic(int maxLength);

  /**
   * @param min
   *          the min length of random string to create
   * @param max
   *          the max length of random string to create
   * @return random string, not null
   */
  String randomAlphabetic(int min,
                          int max);

  /**
   * @return random numeric string, not null (length 10)
   */
  String randomNumeric();

  /**
   * @param max
   *          max length
   * @return random numeric string, not null (length 1 to max)
   */
  String randomNumeric(int max);

  /**
   * @param min
   *          min length
   * @param max
   *          max length
   * @return random numeric string, not null (length min to max)
   */
  String randomNumeric(int min,
                       int max);

  /**
   * @return random valid email
   */
  String randomEmail();

  /**
   * @param <E>
   *          -
   * @param enumType
   *          enum class
   * @return random enum
   */
  <E extends Enum<E>> E randomEnum(Class<E> enumType);

  @SuppressWarnings("unchecked")
  <T extends Enum<T>> T randomEnumExcept(final Class<T> enumType,
                                         final T... excluded);

  /**
   * @return random sql date in the past
   */
  java.sql.Date randomPastSqlDate();

  /**
   * @return random timestamp in the past
   */
  Timestamp randomPastTimestamp();

  /**
   * @return random local date time
   */
  LocalDateTime randomPastLocalDateTime();

  /**
   * @return random local date
   */
  LocalDate randomPastLocalDate();

  /**
   * @return random local date time
   */
  ZonedDateTime randomPastZonedDateTime();

  /**
   * @return random big decimal, not null
   */
  BigDecimal randomBigDecimal();

  /**
   * @return not null
   */
  OffsetDateTime randomPastOffsetDateTime();

  /**
   * @return not null
   */
  Calendar randomPastCalendar();

  /**
   * @return not null
   */
  LocalTime randomLocalTime();

  /**
   * @param list
   *          not null
   * @return not null
   */
  <T> T randomOf(List<T> list);

  /**
   * @param objects
   *          not null
   * @return not null
   */
  <T> T randomOf(@SuppressWarnings("unchecked") T... objects);

  byte[] randomBytes();

  /**
   * @return random UUID not null
   */
  UUID randomUuid();

  /**
   * @return random iban.
   */
  String randomIban();

  String randomInterleaved2Of5Barcode();

}
