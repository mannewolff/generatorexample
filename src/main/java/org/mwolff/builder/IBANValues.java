package org.mwolff.builder;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class IBANValues {

  private static final String[] VALUES = {
      "DE74624206537004446000",
      "DE41215547196669504347",
      "DE39475235911997248331",
      "DE89453029758574243625",
      "DE33897965729966822035",
      "DE55598924424207905934",
      "DE45587583328360474652",
      "DE35284054968509351685",
      "DE76794129857551466858",
      "DE48624735433507182508", };

  public static String randomIBAN() {
    return VALUES[RandomTestNumber.randomInt(VALUES.length - 1)];
  }
}
