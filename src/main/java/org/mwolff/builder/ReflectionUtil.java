package org.mwolff.builder;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ReflectionUtil {

  /**
   * Determines all writable properties with a setter method according to the java bean conventions.
   *
   * @param clazz
   *          not null
   * @return The list of writable properties.
   */
  public static List<PropertyDescriptor> determineJavaBeanProperties(final Class<?> clazz) {

    final List<PropertyDescriptor> propertyDescriptors = new ArrayList<>();
    try {
      final BeanInfo info = Introspector.getBeanInfo(clazz);
      final PropertyDescriptor[] properties = info.getPropertyDescriptors();
      for (final PropertyDescriptor propertyDescriptor : properties) {
        if (propertyDescriptor.getWriteMethod() != null) {
          propertyDescriptors.add(propertyDescriptor);
        }
      }
    } catch (final IllegalArgumentException | IntrospectionException cause) {
      //
    }
    return propertyDescriptors;
  }

  /**
   * Determines all fields, no matter if there is getter or setter.
   *
   * @param clazz
   *          not null
   * @return The set of fields declared at all level of class hierarchy.
   */
  public static List<Field> determineAllFields(final Class<?> clazz) {
    return determineAllFieldsRecursive(clazz, new ArrayList<Field>());
  }

  /**
   * @param clazz
   *          the target class to analyze, not null
   * @param fields
   *          not null
   * @return the array of Field objects representing all the declared fields of this class
   */
  private static List<Field> determineAllFieldsRecursive(final Class<?> clazz,
                                                         final List<Field> fields) {

    final Class<?> superClazz = clazz.getSuperclass();
    if (superClazz != null && superClazz != Object.class) {
      determineAllFieldsRecursive(superClazz, fields);
    }
    for (final Field field : clazz.getDeclaredFields()) {
      // exclude for code coverage tools
      if (!field.isSynthetic() && !Modifier.isStatic(field.getModifiers())) {
        fields.add(field);
      }
    }
    return fields;
  }

}
