package org.mwolff.builder;

import java.util.List;
import java.util.Set;

public interface Builder<TYPE> {


  TYPE build();

  /**
   * In some cases we just want a bean with data and not null in each field. The lazy method ensures
   * to have all fields filled with not null data.
   *
   * @return the builder
   */
  Builder<TYPE> lazy();

  /**
   * Short version as one method call for .lazy().build(), if you dont't need the builder itself.
   *
   * @return the builder
   */
  TYPE lazyBuild();

  /**
   * @return list with one instance of built type, not null
   */
  List<TYPE> lazyListBuild();

  /**
   * @return set with one instance of built type, not null
   */
  Set<TYPE> lazySetBuild();

  /**
   * @param numberOfElements
   *          the number of elements to add with random data
   * @return list with one instance of built type, not null
   */
  List<TYPE> lazyListBuild(int numberOfElements);

  /**
   * @param numberOfElements
   *          the number of elements to add with random data
   * @return set with one instance of built type, not null
   */
  Set<TYPE> lazySetBuild(int numberOfElements);

}
