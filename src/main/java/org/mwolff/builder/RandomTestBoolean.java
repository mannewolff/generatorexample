package org.mwolff.builder;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RandomTestBoolean {

  public static Boolean randomBooleanObject() {
    return Boolean.valueOf(randomBoolean());
  }

  public static boolean randomBoolean() {
    return RandomTestNumber.randomInt(1) == 0;
  }

}
