package org.mwolff.example.populator;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mwolff.converter.Converter;
import org.mwolff.converter.GenericConverter;
import org.mwolff.example.data.UserData;
import org.mwolff.example.entities.Customer;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class UserDataPopulatorTest {

    private Customer createCustomer() {
        Customer customer = new Customer();
        customer.setFirstname("Manfred");
        customer.setLastname("Wolff");
        return customer;
    }

    @DisplayName("Test with a valid UserData object.")
    @Test
    void withValidUserData() {

        // given
        UserData userData = new UserData();
        UserDataPopulator populator = new UserDataPopulator();
        Customer customer = createCustomer();

        // when
        populator.convert(customer, userData);

        // then
        assertThat(userData.getPrename(), is("Manfred"));
        assertThat(userData.getName(), is("Wolff"));
    }

    @DisplayName("Test with the generic populator")
    @Test
    void withGenericConverter() {

        // given
        Converter converter = new GenericConverter();
        converter.addPopulator(new UserDataPopulator());
        Customer customer = createCustomer();
        converter.addSource(customer);
        UserData userData = new UserData();

        // when
        converter.convert(userData);

        // then
        assertThat(userData.getPrename(), is("Manfred"));
        assertThat(userData.getName(), is("Wolff"));
    }

}