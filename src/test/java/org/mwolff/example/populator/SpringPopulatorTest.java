package org.mwolff.example.populator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mwolff.converter.GenericConverter;
import org.mwolff.converter.Populator;
import org.mwolff.example.data.UserData;
import org.mwolff.example.entities.Adresse;
import org.mwolff.example.entities.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:application.xml")
public class SpringPopulatorTest {

    @Autowired
    GenericConverter userdataconverter;

    @Test
    public void testConverterNotNull() {
        assertThat(userdataconverter, notNullValue());
        List<Populator> populatorList = (List<Populator>) ReflectionTestUtils.getField(userdataconverter, "populatorlist");
        assertThat(populatorList, notNullValue());
    }

    private Customer createCustomer() {
        Customer customer = new Customer();
        customer.setFirstname("Manfred");
        customer.setLastname("Wolff");
        return customer;
    }
    private Adresse createAdresse() {
        Adresse adresse = new Adresse();
        adresse.setPlz("28197");
        adresse.setOrt("Bremen");
        adresse.setStrasse("Butjadinger Str");
        adresse.setHausnummer("34a");
        return adresse;
    }

    @Test
    public void populateAll() {

        Adresse adresse = createAdresse();
        Customer customer = createCustomer();
        userdataconverter.addSource(adresse);
        userdataconverter.addSource(customer);

        UserData userData = new UserData();

        // when
        userdataconverter.convert(userData);

        // then
        assertThat(userData.getPrename(), is("Manfred"));
        assertThat(userData.getName(), is("Wolff"));

        assertThat(userData.getMyplz(), is("28197"));
        assertThat(userData.getMyort(), is("Bremen"));
        assertThat(userData.getMystrasse(), is("Butjadinger Str"));
        assertThat(userData.getMyhausnummer(), is("34a"));

    }

}
