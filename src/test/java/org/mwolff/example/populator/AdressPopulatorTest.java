package org.mwolff.example.populator;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mwolff.converter.Converter;
import org.mwolff.converter.GenericConverter;
import org.mwolff.converter.Populator;
import org.mwolff.example.data.UserData;
import org.mwolff.example.entities.Adresse;
import org.mwolff.example.entities.Customer;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class AdressPopulatorTest {

    private Customer createCustomer() {
        Customer customer = Customer.getNewInstance();
        customer.setFirstname("Manfred");
        customer.setLastname("Wolff");
        return customer;
    }
    private Adresse createAdresse() {
        Adresse adresse = Adresse.getNewInstance();
        adresse.setPlz("28197");
        adresse.setOrt("Bremen");
        adresse.setStrasse("Butjadinger Str");
        adresse.setHausnummer("34a");
        return adresse;
    }

    @DisplayName("Test with a valid UserData object.")
    @Test
    void withValidUserData() {

        // given
        UserData userData = UserData.getNewInstance();
        AdressPopulator populator = new AdressPopulator();
        Adresse adresse = createAdresse();

        // when
        populator.convert(adresse, userData);

        // then
        assertThat(userData.getMyplz(), is("28197"));
        assertThat(userData.getMyort(), is("Bremen"));
        assertThat(userData.getMystrasse(), is("Butjadinger Str"));
        assertThat(userData.getMyhausnummer(), is("34a"));
    }

    @DisplayName("Test with the generic populator")
    @Test
    void withGenericConverter() {

        // given
        GenericConverter converter = new GenericConverter();
        List<Populator> popList = new ArrayList<>();
        popList.add(new AdressPopulator());
        converter.setPopulatorlist(popList);

        Adresse adresse = createAdresse();
        converter.addSource(adresse);
        UserData userData = new UserData();

        // when
        converter.convert(userData);

        // then
        assertThat(userData.getMyplz(), is("28197"));
        assertThat(userData.getMyort(), is("Bremen"));
        assertThat(userData.getMystrasse(), is("Butjadinger Str"));
        assertThat(userData.getMyhausnummer(), is("34a"));
    }

    @DisplayName("Test with both, Adress and Namedata")
    @Test
    void integration() {

        Converter converter = new GenericConverter();
        converter.addPopulator(new AdressPopulator());
        converter.addPopulator(new UserDataPopulator());

        Adresse adresse = createAdresse();
        Customer customer = createCustomer();
        converter.addSource(adresse);
        converter.addSource(customer);

        UserData userData = new UserData();

        // when
        converter.convert(userData);

        // then
        assertThat(userData.getPrename(), is("Manfred"));
        assertThat(userData.getName(), is("Wolff"));

        assertThat(userData.getMyplz(), is("28197"));
        assertThat(userData.getMyort(), is("Bremen"));
        assertThat(userData.getMystrasse(), is("Butjadinger Str"));
        assertThat(userData.getMyhausnummer(), is("34a"));

    }
}